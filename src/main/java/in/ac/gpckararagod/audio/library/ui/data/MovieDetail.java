/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckararagod.audio.library.ui.data;

import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class MovieDetail {

    private Integer id;
    private String movieName;

    public MovieDetail(Integer id, String movieName) {
        this.id = id;
        this.movieName = movieName;
    }
   

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }
}