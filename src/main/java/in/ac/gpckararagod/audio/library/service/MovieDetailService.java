/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckararagod.audio.library.service;

import in.ac.gpckararagod.audio.library.ui.data.MovieDetail;
import java.util.List;

/**
 *
 * @author student
 */
public interface MovieDetailService {
    public String saveMovieDetail(String movieName);
   public MovieDetail readMovieDetail(Integer id);
    public List<MovieDetail> getAllMovieDetails();
   public String updateMovieDetail(Integer id,String movieName);
    public String deleteMovieDetails(Integer id);

  
    
    
}
