/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckararagod.audio.library.service.Impl;

import in.ac.gpckararagod.audio.library.ui.data.MovieDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import in.ac.gpckararagod.audio.library.service.MovieDetailService;

/**
 *
 * @author student
 */
public class MovieDetailServicesImpl extends ConnectionServicesImpl implements MovieDetailService {

    @Override
      public List<MovieDetail> getAllMovieDetails() {
        List<MovieDetail> movieDetails = new ArrayList<>();
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "SELECT * FROM MOVIE_DETAILS";
        ResultSet resultSet = statement.executeQuery(query);
        
        while(resultSet.next()){
             Integer id = resultSet.getInt("ID");
                
                String movieName = resultSet.getString("MOVIE_NAME");
                MovieDetail movieDetail = new MovieDetail(id,movieName);
                movieDetails.add(movieDetail);
        }
        
    }   catch (SQLException ex) {
            Logger.getLogger(MovieDetailServicesImpl.class.getName()).log(Level.SEVERE, null, ex);
    }
        return movieDetails;
    
    }



    public String deleteMovieDetail(Integer id) {
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM MOVIE_DETAILS WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if(delete !=1)
                return "Delete failed";
            else
                return "Deleted successfully";
            
        } catch (SQLException ex) {
           return "Delete failed";
        }
    }

    @Override
    public String saveMovieDetail(String movieName) {
       try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = " INSERT INTO MOVIE_DETAILS (MOVIE_NAME) VALUES "
                    + "('"+movieName+"')";
            System.err.println("Query:" + query);
            int status = statement.executeUpdate(query);
            if (status != 1) {
                return "Save failed";
            } else {
                return "Saved sucessfully";
            }
        } catch (SQLException ex) {
            Logger.getLogger(MovieDetailServicesImpl.class.getName()).log(Level.SEVERE, null, ex);
            return "Save failed";

        }
    }

    @Override
    public MovieDetail readMovieDetail(Integer id) {
        MovieDetail movieDetail = null;
            try {
                Connection connection = getConnection();
                Statement statement = connection.createStatement();
            String query = "SELECT * FROM MOVIE_DETAILS WHERE ID="+id;
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                String movieName = resultSet.getString("MOVIE_NAME");
             movieDetail = new MovieDetail(id,movieName);   
            }
            } catch (SQLException ex) {
             
                Logger.getLogger(MovieDetailServicesImpl.class.getName()).log(Level.SEVERE, null, ex);
                
            }   
        return movieDetail;
    }

    @Override
    public String updateMovieDetail(Integer id,String movieName){
       try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "UPDATE MOVIE_DETAILS SET MOVIE_NAME ='"+movieName+"'WHERE ID="+id;
        System.out.print(query);
        int update = statement.executeUpdate(query);
        if(update !=1)
            return "Update failed";
        else
            return "Updated successfully";
        }catch(SQLException ex){
            return "Update failed";
        } 
    }

    @Override
   public String deleteMovieDetails(Integer id) {
            try {
                Connection connection = getConnection();
                Statement statement = connection.createStatement();
            String query = "Delete FROM MOVIE_DETAILS WHERE ID="+id;
            int update = statement.executeUpdate(query);
        if(update !=1)
            return "Delete failed";
        else
            return "Delete  successfully";
        }catch(SQLException ex){
            return "Delete failed";
        }  
        
    }

    

}
